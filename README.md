# TCD Machine Learning 2019-2020 Group Project: Income Prediction

This is Team 51's code for [this private Kaggle competition](https://www.kaggle.com/c/tcd-ml-comp-201920-income-pred-group/overview).

It uses [scikit-learn](https://scikit-learn.org/stable/) for target encoding, and [LightGBM](https://github.com/microsoft/LightGBM) for predictions using gradient-boosting decision trees.


## Setup

Install requirements from `requirements.txt`:

```bash
pip install -r requirements.txt
```

## Running

There are three main pipeline stages: preprocessing, training, and predicting.

### Preprocessing

Given training and test data, preprocess it into a form edible by a model.

Broadly, the following approach is used:

* Drop a number of non-useful features. Some initial data analysis indicates which features can be dropped - see the [IPython notebook](./Kaggle_Group_Project_Data_Analysis.ipynb) for more details. By default, no columns are dropped.
* Convert/coerce a number of stringy columns to numeric.
* Fill NA values with median.
* Target-encode categorical data.
* Rename columns to `x0...xn` and `y0...yn` to avoid issues with disallowed characters in column names.
* Write output to a new file.

Example usage:

```bash
./main.py preprocess --train train.csv --test test.csv --output preprocessed.csv
```

The above invocation would create `test_enc_preprocessed.csv` and `train_enc_preprocessed.csv`, respectively.

### Training

Training a model requires preprocessed input data from the previous step.

Once completed, the following invocation will train a model using LightGBM:

```bash
./main.py train --train train_enc_preprocessed.csv --output model.txt --rounds 100
```

This will perform 100 epochs of training on the given input data, and save the resulting model to `model.txt`. 

LightGBM can take advantage of GPU acceleration, and is set to do by default in this project. If you do not have GPU acceleration available, you may wish to increase the learning rate (set to `0.001` by default), and use a smaller number of epochs.

See [here](https://lightgbm.readthedocs.io/en/latest/GPU-Tutorial.html) for setup details, which may vary according to your platform.

### Prediction

Once a model is trained, the following invocation will predict values given a **preprocessed** input file:

```bash
/main.py predict --model model.txt --test test_enc_preprocessed.csv --output predictions.csv
```

The above invocation will load the model `model.txt`, read the data from `test_enc_preprocessed.csv`, and write the resulting predictions to `predictions.csv`.
