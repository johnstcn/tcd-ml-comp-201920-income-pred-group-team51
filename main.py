#!/usr/bin/env python3

import argparse

import numpy as np
import pandas as pd
import lightgbm as lgb

from sklearn.metrics import mean_absolute_error, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from category_encoders import TargetEncoder

# Drop following columns
DROP_COLS = []

# replace values in these columns with 'unknown
CAT_REPLACE_UNKNOWN = {
    "Satisfation with employer": [np.nan],
    "Gender": [np.nan],
}

# explicitly coerce these columns to numeric dtypes
NUMERIC_COLS = [
    "Year of Record",
    "Work Experience in Current Job [years]",
    "Age",
]

# the following columns are to be target-encoded
CAT_COLS = [
    "Housing Situation",
    "Satisfation with employer",
    "Gender",
    "Country",
    "Hair Color",
    "University Degree",
    "Profession",
]

# catnames convert the column names of dataframe df to x0..xn for all columns not in ys + ignore_cols,
# and y0...yn for all columns not in ignore_cols.
def catnames(df, ys=[], ignore_cols=[]):
    xn = 0
    yn = 0
    out = {}
    for c in df.columns:
        if c in ignore_cols:
            continue
        elif c in ys:
            out[c] = "y{0}".format(yn)
            yn += 1
        else:
            out[c] = "x{0}".format(xn)
            xn += 1
    return out


# preprocess preprocesses the given input data and writes it to the specified output files.
def preprocess(args):
    if not args.train:
        print("specify train csv")
        exit(1)
    if not args.test:
        print("specify test csv")
        exit(1)
    if not args.output:
        print("specify output csv")
        exit(1)

    df_train = pd.read_csv(args.train)
    df_test = pd.read_csv(args.test)

    df_train.drop(columns=DROP_COLS, inplace=True, errors="ignore")
    df_test.drop(columns=DROP_COLS, inplace=True, errors="ignore")

    # convert stringy columns to numeric values
    # special case: extra income: strip trailing EUR
    inx = "Yearly Income in addition to Salary (e.g. Rental Income)"
    df_train[inx] = df_train[inx].str.strip(" EUR").astype("float")
    df_test[inx] = df_test[inx].str.strip(" EUR").astype("float")
    for c in NUMERIC_COLS:
        df_train[c] = pd.to_numeric(df_train[c], errors="coerce")
        df_test[c] = pd.to_numeric(df_test[c], errors="coerce")

    # fill some numeric NA values
    df_train.fillna(df_train.median(), inplace=True)
    df_test.fillna(df_test.median(), inplace=True)

    enc = TargetEncoder(cols=CAT_COLS, return_df=True)

    df_train_encoded = enc.fit_transform(
        df_train.drop(columns=["Total Yearly Income [EUR]"]),
        df_train[["Total Yearly Income [EUR]"]],
    )
    df_test_encoded = enc.transform(df_test.drop(columns=["Total Yearly Income [EUR]"]))
    df_train_encoded["Total Yearly Income [EUR]"] = df_train[
        "Total Yearly Income [EUR]"
    ]

    # some column names have characters in them that make lgbm and friends complain, so just rename them.
    colnames = catnames(
        df_train, ys=["Total Yearly Income [EUR]"], ignore_cols=["Instance"]
    )
    df_train_encoded.rename(columns=colnames, inplace=True)
    df_test_encoded.rename(columns=colnames, inplace=True)

    df_train_encoded.to_csv("train_enc_" + args.output, index=False)
    df_test_encoded.to_csv("test_enc_" + args.output, index=False)


# train trains a model given a preprocessed training set and dumps it to the specified output file.
def train(args):
    if not args.train:
        print("specify preprocessed train csv")
        exit(1)
    if not args.output:
        print("specify output model")
        exit(1)

    df_train = pd.read_csv(args.train)
    xs = df_train.drop(columns=["Instance", "y0"])
    ys = df_train[["y0"]]

    xs_train, xs_test, ys_train, ys_test = train_test_split(xs, ys)

    ds_train = lgb.Dataset(
        xs_train,
        label=ys_train,
        feature_name=list(xs_train.columns),
        categorical_feature="auto",
    )
    params = {
        "task": "train",
        "objective": "regression",
        "device_type": "gpu",
        "num_threads": 2,
        "learning_rate": 0.001,
    }
    lgb_reg = lgb.train(params, ds_train, args.rounds)
    ys_pred = lgb_reg.predict(xs_test)
    mae = mean_absolute_error(ys_pred, ys_test)
    print("MAE: {0:2f}".format(mae))

    lgb_reg.save_model(args.output)


# predict predicts values from the given preprocessed input csv using the specified model, and
# writes the output to the specified file.
def predict(args):
    if not args.model:
        print("specify model")
        exit(1)
    if not args.test:
        print("specify preprocessed csv")
        exit(1)
    if not args.output:
        print("specify output csv")
        exit(1)

    df = pd.read_csv(args.test)
    xs = df.drop(columns=["Instance", "y0"], errors="ignore")

    bst = lgb.Booster(model_file=args.model)
    ys_pred = bst.predict(xs)

    df_out = df[["Instance"]]
    df_out["Total Yearly Income [EUR]"] = ys_pred
    df_out.to_csv(args.output, index=False)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("action", choices=["preprocess", "train", "predict"])
    parser.add_argument("--model")
    parser.add_argument("--train")
    parser.add_argument("--test")
    parser.add_argument("--output")
    parser.add_argument("--cv", action="store_true")
    parser.add_argument("--rounds", type=int, default=10)
    args = parser.parse_args()

    if args.action == "preprocess":
        preprocess(args)
    elif args.action == "train":
        train(args)
    elif args.action == "predict":
        predict(args)


if __name__ == "__main__":
    main()
